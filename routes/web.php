<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataOrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/404', function () {
    return view('welcome');
});

Route::get('/keranjang-kosong', function(){
	return view('main.keranjang-kosong');
})->middleware('verified');

Route::get('/admin', function(){
	return view('admin.adminhome');
})->middleware('verified');

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route Ingredients
Route::get('/admin/ingredient/add', [App\Http\Controllers\IngredientController::class, 'create'])->middleware('auth');

Route::post('/admin/ingredient', [App\Http\Controllers\IngredientController::class, 'store'])->middleware('auth');

Route::get('/admin/ingredient', [App\Http\Controllers\IngredientController::class, 'index'])->middleware('auth');

Route::get('/admin/ingredient/{id}/edit', [App\Http\Controllers\IngredientController::class, 'edit'])->middleware('auth');

Route::put('/admin/ingredient/{id}', [App\Http\Controllers\IngredientController::class, 'update'])->middleware('auth');

Route::delete('/admin/ingredient/{id}', [App\Http\Controllers\IngredientController::class, 'destroy'])->middleware('auth');

Route::get('/admin/ingredient/{id}', [App\Http\Controllers\IngredientController::class, 'show'])->middleware('auth');


//Route Header
//header
Route::get('admin/header', [App\Http\Controllers\HeaderController::class, 'index'])->middleware('auth');
//edit
Route::get('admin/header/{id}/edit', [App\Http\Controllers\HeaderController::class, 'edit'])->middleware('auth');
//save edit
Route::put('admin/header/{id}', [App\Http\Controllers\HeaderController::class, 'update'])->name('save.header');


//Route Categories
Route::get('admin/category/add', [App\Http\Controllers\CategoriesController::class, 'create'])->middleware('auth');

Route::post('admin/category', [App\Http\Controllers\CategoriesController::class, 'store'])->middleware('auth');

Route::get('/admin/category', [App\Http\Controllers\CategoriesController::class, 'index'])->middleware('auth');

Route::get('/admin/category/{id}/edit', [App\Http\Controllers\CategoriesController::class, 'edit'])->middleware('auth');

Route::put('/admin/category/{id}', [App\Http\Controllers\CategoriesController::class, 'update'])->middleware('auth');

Route::delete('/admin/category/{id}', [App\Http\Controllers\CategoriesController::class, 'destroy'])->middleware('auth');




//Route Product
Route::get('/admin/product/add', [App\Http\Controllers\ProductController::class, 'create'])->middleware('auth');

Route::post('/admin/product', [App\Http\Controllers\ProductController::class, 'store'])->middleware('auth');

Route::get('/admin/product', [App\Http\Controllers\ProductController::class, 'index'])->middleware('auth');

Route::get('/admin/product/{id}/edit', [App\Http\Controllers\ProductController::class, 'edit'])->middleware('auth');

Route::put('/admin/product/{id}', [App\Http\Controllers\ProductController::class, 'update'])->middleware('auth');

Route::delete('/admin/product/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->middleware('auth');


//Route Profile
Route::get('/profile/add', [App\Http\Controllers\ProfileController::class, 'create']);
Route::post('/profile', [App\Http\Controllers\ProfileController::class, 'store']);
Route::get('/profile/{id}/edit', [App\Http\Controllers\ProfileController::class, 'edit']);
Route::put('/profile/{id}', [App\Http\Controllers\ProfileController::class, 'update']);
// Route Main
Route::get('/', [App\Http\Controllers\MainController::class, 'index']);
Route::get('/raaina-shop', [App\Http\Controllers\MainController::class, 'product']);
Route::get('/product-detail/{id}', [App\Http\Controllers\MainController::class, 'productDetail']);
Route::get('/product/{id}/checkout', [App\Http\Controllers\MainController::class, 'productBeli']);
Route::post('/product/{id}/checkout', [App\Http\Controllers\MainController::class, 'pesan']);
Route::get('/check-out', [App\Http\Controllers\MainController::class, 'checkout']);
Route::delete('/check-out/{id}', [App\Http\Controllers\MainController::class, 'checkoutDelete']);
Route::get('/confirm-check-out', [App\Http\Controllers\MainController::class, 'checkoutConfirm']);
Route::put('/confirm-check-out', [App\Http\Controllers\MainController::class, 'checkoutConfirmPut']);
Route::get('/check-order', [App\Http\Controllers\MainController::class, 'checkOrder']);
Route::get('/ongkir', [App\Http\Controllers\MainController::class, 'getProvince']);
Route::post('/ongkir', [App\Http\Controllers\MainController::class, 'check_ongkir']);
Route::get('/cities/{province_id}', [App\Http\Controllers\MainController::class, 'getCities']);
Route::post('kota', [App\Http\Controllers\MainController::class, 'kota'])->name('kota');
Route::get('/findKotaName', [App\Http\Controllers\MainController::class, 'findKotaName']);

// Route API
Route::get('/province', [App\Http\Controllers\getApiController::class, 'province']);

// Route Data Order

Route::get('/admin/data-order', [App\Http\Controllers\DataOrderController::class, 'index'])->middleware('auth');

Route::get('/admin/data-order/{id}/edit', [App\Http\Controllers\DataOrderController::class, 'edit'])->middleware('auth');

Route::put('/admin/data-order/{id}', [App\Http\Controllers\DataOrderController::class, 'update'])->middleware('auth');

Route::get('/admin/data-order/{id}', [App\Http\Controllers\DataOrderController::class, 'show'])->middleware('auth');

Route::get('/admin/orders', [App\Http\Controllers\DataOrderController::class, 'orders'])->middleware('auth');

Route::get('/admin/history', [App\Http\Controllers\DataOrderController::class, 'history'])->middleware('auth');

Route::get('/admin/data-order/{id}/upload-bukti-pembayaran', [App\Http\Controllers\DataOrderController::class, 'buktiPembayaran'])->middleware('auth');

Route::put('/admin/upload-bukti/{id}', [App\Http\Controllers\DataOrderController::class, 'upload'])->middleware('auth');


