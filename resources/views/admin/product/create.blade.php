@extends('admin.master')

@section('content')
<div class="ml-3 mt-2 mr-3">
	<div class="card">
		<div class="card-header" style="background-color: pink">
			<h3 class="card-title" style="color: #000">Add New Product</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="/admin/product" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="card-body">

				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" id="name" value="{{old('name','')}}" name="name" >
					@error('name')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="categories_id">Product Category</label>
					<select class="custom-select" id="categories_id" name="categories_id">
						<option value="" selected>Choose...</option>
						@foreach($category as $category2)
						<option id="categories_id" value="{{ $category2->id }}">{{ $category2->name }}</option>
						@endforeach
					</select>
				</div>

				<!-- <div class="form-group">
					<label><strong>Select Ingredients :</strong></label><br/>
					<select class="selectpicker" multiple data-live-search="true" name="cat[]" style="width: 100%">
						@foreach($ingredient as $ingredient2)
						<option value="{{$ingredient2->id}}">{{$ingredient2->name}}</option>
						@endforeach
					</select>
				</div> -->
				<div class="form-group">
					<label>Ingredients</label><br>
					<select class="js-example-basic-multiple" style="width: 100%" name="ingredient[]" multiple="multiple">
						@foreach($ingredient as $ingredient2)
						<option value="{{$ingredient2->id}}">{{$ingredient2->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="qty">Qty</label>
					<input type="text" class="form-control" id="qty" value="{{old('qty','')}}" name="qty" >
					@error('qty')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="dimensi">Product Dimensions</label>
					<input type="text" class="form-control" id="dimensi" value="{{old('dimensi','')}}" name="dimensi" >
					@error('dimensi')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="berat_produk">Weight (gram)</label>
					<input type="number" class="form-control" id="berat_produk" value="{{old('berat_produk','')}}" name="berat_produk" >
					@error('berat_produk')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="manfaat">Product Benefits</label>
					<textarea id="manfaat" class="form-control" value="{{old('manfaat','')}}" name="manfaat" style="height: 10em"></textarea>
					@error('manfaat')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="cara_penggunaan">How to Use</label>
					<textarea id="cara_penggunaan" class="form-control" value="{{old('cara_penggunaan','')}}" name="cara_penggunaan" style="height: 10em"></textarea>
					@error('cara_penggunaan')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="harga">Price</label>
					<input type="number" class="form-control" id="harga" value="{{old('harga','')}}" name="harga" >
					@error('harga')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="stock">Stock</label>
					<select class="custom-select" id="stock" name="stock">
						<option id="stock" value="In Stock">In Stock</option>
						<option id="stock" value="Out of Stock">Out of Stock</option>
					</select>
				</div>

				<div class="form-group">
					<label for="harga">Product Images</label>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<input type="file" class="form-control" id="image1" value="" name="image1" onchange="previewFile(this)">
								@error('image1')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="file" class="form-control" id="image2" value="" name="image2" onchange="previewFile2(this)">
								@error('image2')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="file" class="form-control" id="image3" value="" name="image3" onchange="previewFile3(this)">
								@error('image3')
								<div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn" style="background-color: pink"><i class="fa fa-save"></i> Save</button>
			</div>
		</form>
	</div>
	<!-- Initialize the plugin: -->
	<script>
		$(document).ready(function() {
			$('.js-example-basic-multiple').select2();
		});
	</script>
</div>

@endsection