@extends('admin.master')

@section('content')
<div class="container">
	<a class="btn mt-2 mb-2" style="background-color: pink" href="/admin/product/add"><i class="fa fa-plus-circle"></i> Add New Product</a>
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: pink">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Image</th>
				<th scope="col">Name</th>
				<th scope="col">Category</th>
				<th scope="col">Benefit</th>
				<th scope="col">How To Use</th>
				<th scope="col">Specification</th>
				<th scope="col">Price</th>
				<th scope="col">Ingredients</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<style>
			.table-buttons{
				text-align: center;
			}
			
		</style>
		<tbody class="">
			@forelse($product as $key => $product2)
			<tr>
				<th scope="row">{{$key + $product->firstItem()}}</th>
				<td><img src="{{asset('images/product/'.$product2->image1)}}" style="width: 100px; height: 100px"></td>
				<td>{{$product2->name}}</td>
				<td>{{$product2->categories->name}}</td>
				@if(strlen($product2->manfaat)>80)
				<td style="white-space: pre-line;">{!! substr($product2->manfaat, 0, 20) !!}...</td>
				@else
				<td style="white-space: pre-line;">{!! $product2->manfaat !!}</td>
				@endif

				@if(strlen($product2->cara_penggunaan)>80)
				<td style="white-space: pre-line;">{!! substr($product2->cara_penggunaan, 0, 20) !!}...</td>
				@else
				<td style="white-space: pre-line;">{!! $product2->cara_penggunaan !!}</td>
				@endif
				<td><strong>Qty</strong> : {{$product2->qty}}</p>
					<p><strong>Dimension</strong> : {{$product2->dimensi}}</p>
					<p><strong>Weight</strong> : {{$product2->berat_produk}}</p>
				</td>
				<td>{{$product2->harga}}</td>
				<td>@foreach($product2->ingredients as $ing)
					<div class="row btn btn-sm mb-2 ml-1 mr-1" style="background-color: pink">{{ $ing->name }}</div>
					@endforeach
					
				</td>
				<td width="200px" class="table-buttons">
					<form action="/admin/product/{{$product2->id}}" method="post">
						<a href="/admin/product/{{$product2->id}}" class="btn btn-sm" style="background-color: pink"><i class="fa fa-eye"></i></a>
						<a href="/admin/product/{{$product2->id}}/edit" class="btn btn-sm" style="background-color: pink"><i class="fa fa-edit"></i></a>
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-dark btn-sm">
							<i class="fa fa-trash" style="color: pink"></i>
						</button>
					</form>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="4" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	
	<style>
		p{
			margin-top: 1rem;
		}
	</style>
	{{$product->links()}}
	
</div>

@endsection