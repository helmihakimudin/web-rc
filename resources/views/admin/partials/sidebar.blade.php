<aside class="main-sidebar sidebar-light-primary elevation-4" style="background-color: #decd6f">
  <!-- Brand Logo -->
  <a href="/admin" class="brand-link">
    <img src="{{asset('images/logo-raaina.png')}}" alt="Raaina Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">R A A I N A</span>
  </a> 

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @if(!Auth::user()->profiles)
        <img src="{{ asset('images/logo-raaina.png')}}" class="img-circle elevation-2" alt="User Image" style="height: 2em; width: 2em">
        @else
        <img src="{{ asset('images/profile/'.Auth::user()->profiles->avatar)}}" class="img-circle elevation-2" alt="User Image" style="height: 2em; width: 2em">
        @endif
      </div>
      <div class="info">
        @if(!Auth::user()->profiles)
        <a>{{Auth::user()->name}}</a>
        @else
        <a>{{Auth::user()->profiles->nama_lengkap}}</a>
        @endif
      </div>
    </div>

    <!-- Sidebar Menu -->



    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
         @if(Auth::user()->level != 'administrator')
         <li class="nav-item">
          <a href="/admin" class="nav-link">
            <i class="nav-icon fa fa-home"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          @if(!Auth::user()->profiles)
          <a href="/profile/add" class="nav-link">
            @else
            <a href="/profile/{{Auth::user()->profiles->id}}/edit" class="nav-link">
              @endif
              <i class="nav-icon fa fa-user-circle"></i>
              <p>
                Data Profil
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/orders" class="nav-link">
              <i class="nav-icon fa fa-cart-plus"></i>
              <p>
                Pesanan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/history" class="nav-link">
              <i class="nav-icon fa fa-history"></i>
              <p>
                History Pesanan
              </p>
            </a>
          </li>



          @else
          <li class="nav-item">
            <a href="/admin" class="nav-link">
              <i class="nav-icon fa fa-home"></i>
              <p>
                Home
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview menu-close">
            <a href="/admin/header" class="nav-link">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Header
              </p>
            </a>
          <!-- <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/header/1/edit" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Edit Header</p>
                </a>
              </li>
            </ul> -->
          </li>
          <li class="nav-item has-treeview menu-close">
            <a href="/admin/ingredient" class="nav-link">
              <i class="nav-icon fas fa-flask"></i>
              <p>
                Ingredients
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview menu-close">
            <a href="/admin/category" class="nav-link">
              <i class="nav-icon fas fa-link"></i>
              <p>
                Product Categories
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview menu-close">
            <a href="/admin/product" class="nav-link">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Products
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/admin/data-order" class="nav-link">
              <i class="nav-icon fa fa-shopping-cart"></i>
              <p>
                Data Orders
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">

            <i class="nav-icon fa fa-frown"></i>
            <p>
              {{ __('Logout') }}
            </p>
          </a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>


      </ul>
    </nav>

    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>