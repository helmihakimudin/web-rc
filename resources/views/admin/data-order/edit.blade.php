@extends('admin.master')

@section('content')
<div class="ml-3 mt-2 mr-3">
	<div class="card">
		<div class="card-header" style="background-color: pink ">
			<h3 class="card-title" style="color: #000">Update Order Status</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="/admin/data-order/{{$status->id}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PUT')
			<div class="card-body">

				<div class="form-group">
					<label for="order_status">Order Status</label>
					<select name="order_status" class="form-control">
						<option value="Pembayaran Belum Diterima">Pembayaran Belum Diterima</option>
						<option value="Pembayaran Sudah Diterima">Pembayaran Sudah Diterima</option>
						<option value="Dalam Proses Pengiriman">Dalam Proses Pengiriman</option>
						<option value="Transaksi Selesai">Transaksi Selesai</option>
					</select>

				</div>
			</div>
			<div class="card-footer">
				<button type="submit" class="btn" style="background-color: pink "><i class="fa fa-save"></i> Save</button>
			</div>
		</form>
	</div>
</div>
@endsection