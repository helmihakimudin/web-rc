@extends('admin.master')

@section('content')
<div class="container">
	<div class="col-md-6">
		<table class="table">
			<tr>
				<td>Order Date</td>
				<td> :</td>
				<td>{{$order->created_at->isoFormat('D MMMM Y')}}</td>
			</tr>
			<tr>
				<td>Email Account</td>
				<td> :</td>
				<td>{{$order->user->email}}</td>
			</tr>
			<tr>
				<td>Nama Penerima</td>
				<td> :</td>
				<td>{{$order->nama_penerima}}</td>
			</tr>
			<tr>
				<td>Alamat Pengiriman</td>
				<td> :</td>
				<td>{{$order->alamat}}</td>
			</tr>
			<tr>
				<td>Payment Status</td>
				<td> :</td>
				<td>
					@if(!empty($order->bukti_pembayaran))
					<span style="color: red">Sudah di Upload</span>
					@else
					<span style="color: red">Belum di Upload</span>
					@endif
				</td>
			</tr>
			<tr>
				<td>Order Status</td>
				<td> :</td>
				<td>{{$order->order_status}}</td>
			</tr>
		</table>
	</div>
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: pink">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Item</th>
				<th scope="col">Qty Order</th>
				<th scope="col">Price</th>
				<th scope="col">Total Price</th>
			</tr>
		</thead>
		<tbody>
			@foreach($detail_show as $key=> $detail)
			<tr>
				<td>{{$key + 1}}</td>
				<td>{{$detail->product->name}}</td>
				<td>{{$detail->jumlah_pesanan}}</td>
				<td>@currency($detail->product->harga)</td>
				<td>@currency($detail->total_harga)</td>
			</tr>
			@endforeach
			<tr>
				<td colspan="4" align="right"><strong>Total Price</strong></td>
				<td>@currency($order->total_harga)</td>
			</tr>
		</tbody>
		<style>
			.table-buttons{
				text-align: center;
			}
			
		</style>

	</table>
</div>
@endsection