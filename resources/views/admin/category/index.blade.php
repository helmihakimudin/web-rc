@extends('admin.master')

@section('content')
<div class="container">
	<a class="btn mt-2 mb-2" style="background-color: #ff469d " href="/admin/category/add"><i class="fa fa-plus-circle"></i> Add New Category</a>
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: #ff469d ">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Name</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<style>
			.table-buttons{
				text-align: left;
			}
			
		</style>
		<tbody class="">
			@forelse($category as $key => $category2)
			<tr>
				<th scope="row">{{$key + $category->firstItem()}}</th>
				<td>{{$category2->name}}</td>
				<td width="180px" class="table-buttons">
					<form action="/admin/category/{{$category2->id}}" method="post">
					<a href="/admin/category/{{$category2->id}}/edit" class="btn" style="background-color: #ff469d "><i class="fa fa-edit"></i></a>
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-dark">
							<i class="fa fa-trash" style="color: #ff469d "></i>
						</button>
					</form>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="4" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	
					<style>
						p{
							margin-top: 1rem;
						}
						</style>
	{{$category->links()}}
</div>

@endsection