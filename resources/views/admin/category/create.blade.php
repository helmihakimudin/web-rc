@extends('admin.master')

@section('content')
<div class="ml-3 mt-2 mr-3">
	<div class="card">
		<div class="card-header" style="background-color: #ff469d ">
			<h3 class="card-title" style="color: #000">Add New Category</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="/admin/category" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="card-body">

				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" id="name" value="{{old('name','')}}" name="name" >
					@error('name')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>
			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn" style="background-color: #ff469d "><i class="fa fa-save"></i> Save</button>
			</div>
		</form>
	</div>
</div>
@endsection