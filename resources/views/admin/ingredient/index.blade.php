@extends('admin.master')

@section('content')
<div class="container">
	<a class="btn mt-2 mb-2" style="background-color: pink" href="/admin/ingredient/add"><i class="fa fa-plus-circle"></i> Add New Ingredient</a>
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: pink">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Name</th>
				<th scope="col">Description</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<style>
			.table-buttons{
				text-align: center;
			}
			
		</style>
		<tbody class="">
			@forelse($ingredient as $key => $ingredient2)
			<tr>
				<th scope="row">{{$key + $ingredient->firstItem()}}</th>
				<td>{{$ingredient2->name}}</td>

				@if(strlen($ingredient2->description)>80)
				<td style="white-space: pre-line;">{!! substr($ingredient2->description, 0, 20) !!}...</td>
				@else
				<td style="white-space: pre-line;">{!! $ingredient2->description !!}</td>
				@endif
				<td width="180px" class="table-buttons">
					<form action="/admin/ingredient/{{$ingredient2->id}}" method="post">
					<a href="/admin/ingredient/{{$ingredient2->id}}" class="btn mb-1" style="background-color: pink"><i class="fa fa-eye"></i></a>
					<a href="/admin/ingredient/{{$ingredient2->id}}/edit" class="btn mb-1" style="background-color: pink"><i class="fa fa-edit"></i></a>
						@csrf
						@method('DELETE')
						<button type="submit" class="btn btn-dark mb-1">
							<i class="fa fa-trash" style="color: pink"></i>
						</button>
					</form>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="4" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	
					<style>
						p{
							margin-top: 1rem;
						}
						</style>
	{{$ingredient->links()}}
</div>

@endsection