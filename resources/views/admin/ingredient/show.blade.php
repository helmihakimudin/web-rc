@extends('admin.master')

@section('content')
<div class="container mt-3">
	<div class="card">
		<div class="card-header" style="background-color: pink; font-weight: bold;">
			Detail Information
		</div>
		<div class="card-body">
			<h3 class=""> {{$ingredient->name}}</h3>
			<p class="card-text">{{$ingredient->description}}</p>
			<!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
		</div>
	</div>
</div>

@endsection