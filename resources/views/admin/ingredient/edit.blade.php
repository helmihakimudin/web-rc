@extends('admin.master')

@section('content')
<div class="ml-3 mt-2 mr-3">
	<div class="card">
		<div class="card-header" style="background-color: pink">
			<h3 class="card-title" style="color: #000">Edit Ingredients : {{ $ingredient->name }}</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="/admin/ingredient/{{$ingredient->id}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PUT')
			<div class="card-body">

				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" id="name" value="{{old('name', $ingredient->name)}}" name="name" >
					@error('name')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<textarea id="description" class="form-control" value="{{old('description', $ingredient->description)}}" name="description" style="height: 10em">{{$ingredient->description}}</textarea>
					@error('description')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn" style="background-color: pink"><i class="fa fa-save"></i> Save</button>
			</div>
		</form>
	</div>
</div>
@endsection