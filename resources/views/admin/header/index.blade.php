@extends('admin.master')

@section('content')
<div class="container">
	<div class="card-body">
		@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif
		<h3 class="card-title mb-3" style="font-size: 30px">Halaman Header</h3>

		<table class="table table-bordered">
			<thead class="table-active bordered" style="background-color: pink">
				<tr>
					<th scope="col" width="60px">No</th>
					<th scope="col">Image</th>
					<th scope="col">Action</th>
				</tr>
			</thead>

			<tbody class="">
				@forelse($header as $key => $header2)
				<tr>
					<th scope="row">{{$key + 1}}</th>
					<td><img src="{{asset('images/header/'.$header2->images)}}" style="width: 100%; height: 20em; justify-content: center; border-radius: 10px"></td>
					<th width="20px">
						<!-- <a href="/header/{{$header2->id}}" class="btn btn-info btn-sm" style="width: 4em; border-radius: 10px">Detail</a> -->
						<a href="/admin/header/{{$header2->id}}/edit" class="btn" style="background-color: pink"><i class="fas fa-edit" style="justify-content: center; color: black" ></i></a>
					<!-- <form action="/header/{{$header2->id}}" method="post">
						@csrf
						@method('DELETE')
						<input type="submit" value="Delete" class="btn btn-danger btn-sm mt-1" style="width: 4em; border-radius: 10px">
					</form> -->
				</th>
			</tr>
			@empty
			<tr>
				<td colspan="4" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	
	<style>
		.page-item.active .page-link {
			z-index: 3;
			color: #fff;
			background-color: #e03a3c;
			border-color: #e03a3c;
		}

		.page-item .page-link {
			z-index: 3;
			color: #e03a3c;

		}
	</style>
	
</div>
@endsection