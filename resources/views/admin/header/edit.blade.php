@extends('admin.master')

@section('content')

<div class="ml-3 mt-2 mr-3">
  <div class="card card-light">
    <div class="card-header" style="background-color: pink">
      <h3 class="card-title">Edit Header <span style="color: blue">{{$header->id}}</span></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/admin/header/{{$header->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="card-body">

       <div class="form-group">
        <label for="images">Image</label>
        <input type="file" class="form-control" id="images" value="" name="images" onchange="previewFile(this)">
        @error('images')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <img id="previewImg" src="{{asset('images/header/'.$header->images)}}" height="200" width="350" style="border-radius: 10px"/>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn" style="background-color: pink"><i class="fa fa-save"></i> Save</button>
    </div>
  </form>
</div>
</div>

<script>
 function previewFile(input){
  var file=$("input[type=file]").get(0).files[0];
  if(file)
  {
    var reader = new FileReader();
    reader.onload = function(){
      $('#previewImg').attr("src",reader.result);
    }
    reader.readAsDataURL(file);
  }
}
</script>
@endsection