@extends('admin.master')

@section('content')
<div class="ml-3 mt-2 mr-3">
	<div class="card">
		<div class="card-header" style="background-color: pink">
			<h3 class="card-title" style="color: #000">Tambah Data Diri</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('put')
			<div class="card-body">

				<div class="form-group">
					<label for="nama_lengkap">Nama Lengkap</label>
					<input type="text" class="form-control" id="nama_lengkap" value="{{old('nama_lengkap',$profile->nama_lengkap)}}" name="nama_lengkap" >
					@error('nama_lengkap')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="tanggal_lahir">Tanggal Lahir</label>
					<input type="date" class="form-control" id="tanggal_lahir" value="{{old('tanggal_lahir',$profile->tanggal_lahir)}}" name="tanggal_lahir" >
					@error('tanggal_lahir')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="no_hp">Nomor Telpon</label>
					<input type="number" class="form-control" id="no_hp" value="{{old('no_hp',$profile->no_hp)}}" name="no_hp" >
					@error('no_hp')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="alamat_pengiriman">Alamat Pengiriman</label>
					<textarea id="alamat_pengiriman" class="form-control" value="{{old('alamat_pengiriman',$profile->alamat_pengiriman)}}" name="alamat_pengiriman" style="height: 10em">{{$profile->alamat_pengiriman}}</textarea>
					@error('alamat_pengiriman')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>

				<div class="form-group">
					<label for="avatar">Foto Profil</label>
					<input type="file" class="form-control" id="avatar" value="$profile->avatar" name="avatar" onchange="previewFile(this)">
					@error('avatar')
					<div class="alert alert-danger">{{ $message }}</div>
					@enderror
				</div>
				<img id="previewImg" src="{{asset('images/profile/'.$profile->avatar)}}" height="200" width="350" style="border-radius: 10px; border: none;"/>
			</div>
			<!-- /.card-body -->

			<div class="card-footer">
				<button type="submit" class="btn" style="background-color: pink"><i class="fa fa-save"></i> Save</button>
			</div>
		</form>
	</div>
</div>
<script>
	function previewFile(input){
		var file=$("input[type=file]").get(0).files[0];

		if(file)
		{
			var reader = new FileReader();
			reader.onload = function(){
				$('#previewImg').attr("src",reader.result);
			}
			reader.readAsDataURL(file);
		}
	}
</script>
@endsection