@extends('admin.master')

@section('content')
<div class="container mt-4">
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: pink">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Date</th>
				<th scope="col">Akun</th>
				
				<th scope="col">Total Price</th>
				<th scope="col">Bukti Pembayaran</th>
				<th scope="col">Status</th>
				
				<th scope="col">Action</th>
			</tr>
		</thead>
		<style>
			.table-buttons{
				text-align: center;
			}
			
		</style>
		<tbody class="">
			@forelse($order as $key => $order2)
			<tr>
				<th scope="row">{{$key + $order->firstItem()}}</th>
				<td>{{$order2->created_at->isoFormat('D MMMM Y')}}</td>
				<td>{{$order2->user->email}}</td>
				<td>@currency($order2->total_harga)</td>
				<td>
					@if(!empty($order2->bukti_pembayaran))
					<img src="{{asset('images/bukti/'.$order2->bukti_pembayaran)}}" height="100" width="200">
					@endif
				</td>
				<td>@if(!empty($order2->bukti_pembayaran) && $order2->order_status == 'Pembayaran Belum Diterima')
					Proses Verifikasi Pembayaran
					@else
					{{$order2->order_status}}
					@endif
				</td>
				
				<td width="180px" class="table-buttons">
					<!-- <form action="/admin/ingredient/{{$order2->id}}" method="post"> -->
						<a href="/admin/data-order/{{$order2->id}}" class="btn mb-1" style="background-color: pink"><i class="fa fa-eye"></i></a>
						<a href="/admin/data-order/{{$order2->id}}/upload-bukti-pembayaran" class="btn mb-1" style="background-color: pink"><i class="fa fa-edit"></i></a>
						<!-- @csrf
						@method('DELETE')
						<button type="submit" class="btn btn-dark mb-1">
							<i class="fa fa-trash" style="color: pink"></i>
						</button> -->
					</form>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="7" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	
	<style>
		p{
			margin-top: 1rem;
		}
	</style>
	{{$order->links()}}
</div>

@endsection