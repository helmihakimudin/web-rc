@extends('admin.master')

@section('content')
<div class="container mt-4">
	<table class="table table-bordered">
		<thead class="table-active bordered" style="background-color: pink">
			<tr>
				<th scope="col" width="60px">No</th>
				<th scope="col">Date</th>
				<th scope="col">Akun</th>
				<th scope="col">Total Price</th>
				<th scope="col">Proof of Payment</th>
				<th scope="col">Status</th>
				
			</tr>
		</thead>
		<style>
			.table-buttons{
				text-align: center;
			}
			
		</style>
		
		<tbody class="">
			@forelse($history as $key => $order2)
			<tr>
				<th scope="row">{{$key + 1}}</th>
				<td>{{date('d-m-Y', strtotime($order2->created_at))}}</td>
				<td>{{Auth::user()->email}}</td>
				<td>@currency($order2->total_harga)</td>
				<td><img src="{{asset('images/bukti/'.$order2->bukti_pembayaran)}}" height="100" width="200"></td>
				<td>{{$order2->order_status}}</td>
			</tr>
			@empty
			<tr>
				<td colspan="7" align="center">Data Masih Kosong</td>
			</tr>
		</tbody>
		@endforelse
	</table>
	


</div>

@endsection