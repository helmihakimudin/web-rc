@extends('admin.master')

@section('content')

<div class="ml-3 mt-2 mr-3">
  <div class="card card-light">
    <div class="card-header" style="background-color: pink">
      <h3 class="card-title">Upload Bukti Pembayaran </h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/admin/upload-bukti/{{$bukti->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="card-body">

       <div class="form-group">
        <label for="bukti_pembayaran">Bukti Pembayaran</label>
        <input type="file" class="form-control" id="bukti_pembayaran" value="" name="bukti_pembayaran" onchange="previewFile(this)">
        @error('bukti_pembayaran')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <img id="previewImg" src="{{asset('images/bukti_pembayaran/'.$bukti->bukti_pembayaran)}}" height="200" width="350" style="border-radius: 10px"/>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn" style="background-color: pink"><i class="fa fa-save"></i> Save</button>
    </div>
  </form>
</div>
</div>

<script>
 function previewFile(input){
  var file=$("input[type=file]").get(0).files[0];
  if(file)
  {
    var reader = new FileReader();
    reader.onload = function(){
      $('#previewImg').attr("src",reader.result);
    }
    reader.readAsDataURL(file);
  }
}
</script>
@endsection