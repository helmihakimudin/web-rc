@extends('admin.master')

@section('content')
<div class="ml-4 mt-2">
<h4>Selamat Datang, <span style="color: red;">
@if(!Auth::user()->profiles)
{{Auth::user()->name}}
@else
{{Auth::user()->profiles->nama_lengkap}}
@endif
</span></h4>
<br>
<h5 style="color: red; text-decoration: underline;">Reminder</h5>
<p>Jangan pernah bosan untuk menebar kebaikan, Keep Istiqomah !</p>
</div>

<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-12">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3></h3>

                <p>Artikel Islami</p>
              </div>
              <div class="icon">
                <i class="ion ion-folder"></i>
              </div>
              <a href="/blog/index" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-12">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3></h3>

                <p>Jadwal Pengajian</p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
              <a href="/event/index" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-12">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">

                <h3></h3>

                <p>Jumlah Pengguna</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <!-- <div class="col-lg-3 col-6"> -->
            <!-- small box -->
       <!--      <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div> -->
          <!-- ./col -->
        </div>
    
</section>
        <!-- /.row -->
     

@endsection