@extends('main.master2')

@section('content')
<main id="main">

	<!-- ======= Breadcrumbs Section ======= -->
	<section class="breadcrumbs">
		<div class="container">

			<div class="d-flex justify-content-between align-items-center">
				<h2>Product Information of {{$product->name}}</h2>
				<ol>
					<li><a href="/">Home</a></li>
					<li><a href="/our-products">Our Products</a></li>
					<li>Product Information</li>
				</ol>
			</div>

		</div>
	</section><!-- Breadcrumbs Section -->

	<!-- ======= Portfolio Details Section ======= -->
	<section class="portfolio-details">
		<div class="container">

			<div class="portfolio-details-container">
				<div class="owl-carousel portfolio-details-carousel">
					@if(!$product->image2 && !$product->image3)
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 500px">
					@elseif(!$product->image3)
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 500px">
					<img src="{{asset('images/product/'.$product->image2)}}" class="img-fluid" alt="" style="height: 500px">
					@else
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 500px">
					<img src="{{asset('images/product/'.$product->image2)}}" class="img-fluid" alt="" style="height: 500px">
					<img src="{{asset('images/product/'.$product->image3)}}" class="img-fluid" alt="" style="height: 500px">
					@endif
				</div>

				<div class="portfolio-info">
					<h3>Product information</h3>
					<ul>
						<li><strong>Kategori </strong>: {{$product->categories->name}}</li>
						<li><strong>Nama Produk </strong>: {{$product->name}}</li>
						<li><strong>Harga </strong>: @currency($product->harga)</li>
						<li><strong>Tersedia juga di </strong>: 
							<a href="#">Shopee,</a>
							<a href="#">Tokopedia,</a>
							<a href="#">Bukalapak</a>
						</li>
						<li class="dropdown-divider"></li>
						<li><a href="" class="btn btn-sm float-right mt-2" style="border-radius: 10px; color: white; background-color:#bc8d42 "><i class="fa fa-cart-plus"></i> Beli Produk</a></li>
					</ul>
				</div>

			</div>

			<div class="portfolio-description">
				<h2>{{$product->name}}</h2>
			</div>
			<style>
				.accordion {
					background-color: pink;
					color: #444;
					cursor: pointer;
					padding: 18px;
					width: 100%;
					border: none;
					text-align: left;
					outline: none;
					font-size: 15px;
					transition: 0.4s;
					
				}

				.accordion:hover {
					background-color: #ff469d; 
				}

				.panel {
					padding: 0 18px;
					display: none;
					background-color: white;
					overflow: hidden;
				}
			</style>

			<button class="accordion"><strong>Komposisi Bahan Produk</strong> <i class="fa fa-plus-circle float-right"></i></button>
			<div class="panel">
				<p class="mt-2">@foreach($product->ingredients as $ing)
					<div class="row btn btn-sm mb-2 ml-1 mr-1" style="background-color: pink">{{ $ing->name }}</div>
				@endforeach</p>
			</div>
			<li class="dropdown-divider"></li>
			<button class="accordion "><strong>Manfaat/Khasiat Produk </strong> <i class="fa fa-plus-circle float-right"></i></button>
			<div class="panel">
				<p class="mt-2">{!!$product->manfaat!!}</p>
			</div>
			<li class="dropdown-divider"></li>
			<button class="accordion"><strong>Cara Penggunaan Produk</strong> <i class="fa fa-plus-circle float-right"></i></button>
			<div class="panel">
				<p class="mt-2">{!! $product->cara_penggunaan !!}</p>
			</div>
			<li class="dropdown-divider"></li>
			<button class="accordion"><strong>Spesifikasi Produk</strong> <i class="fa fa-plus-circle float-right"></i></button>
			<div class="panel">
				<table class="table" style="border-top: hidden;">
					<tr>
						<td>Dimensi</td>
						<td>:</td>
						<td>{{$product->dimensi}}</td>
					</tr>
					<tr>
						<td>Isi</td>
						<td>:</td>
						<td>{{$product->qty}}</td>
					</tr>
					<tr>
						<td>Berat Produk</td>
						<td>:</td>
						<td>{{$product->berat_produk}}</td>
					</tr>
				</table>
			</div>

			<script>
				var acc = document.getElementsByClassName("accordion");
				var i;

				for (i = 0; i < acc.length; i++) {
					acc[i].addEventListener("click", function() {
						this.classList.toggle("active");
						var panel = this.nextElementSibling;
						if (panel.style.display === "block") {
							panel.style.display = "none";
						} else {
							panel.style.display = "block";
						}
					});
				}
			</script>

		</div>
	</section><!-- End Portfolio Details Section -->

</main><!-- End #main -->
@endsection