<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Raaina</title>
  <link rel="icon" href="{{asset('images/logo-raaina.png')}}">
  <meta content="" name="description">
  <meta content="" name="keywords">

  
  <!-- Favicons -->
  <link href="{{asset('/asset2/assets/img/favicon.png')}}" rel="icon">
  <link href="{{asset('/asset2/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('/asset2/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('/asset2/assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link rel="stylesheet" href=" {{ asset('/plugins/fontawesome-free/css/all.min.css')}} ">

  <!-- Template Main CSS File -->
  <link href="{{asset('/asset2/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Mamba - v2.4.1
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
   <!-- Include this in your blade layout -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
</head>

<body>
  @include('main.partials.navbar2')

  @yield('content')
  @include('sweet::alert')

  <!-- ======= Footer ======= -->
  
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset('asset2/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/jquery-sticky/jquery.sticky.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('asset2/assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('asset2/assets/js/main.js')}}"></script>


</body>

</html>