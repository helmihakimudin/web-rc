@extends('main.master2')

@section('content')
<!-- ======= Breadcrumbs Section ======= -->
<section class="breadcrumbs">
	<div class="container">

		<div class="d-flex justify-content-between align-items-center">
			<h2 style="font-weight: bold; text-shadow: 1px 1px 0.3px #000000; color: #bc8d42">Order Page</h2>
			<ol>
				<li><a href="/">Home</a></li>
				<li><a href="/our-products">Our Products</a></li>
				<li style="color: #bc8d42">Order Page</li>
			</ol>
		</div>

	</div>
</section><!-- End Breadcrumbs Section -->
<div class="container mt-5">
	<div class="row">
		<div class="col-md-6">
			<div class="portfolio-details-container">
				<div class="owl-carousel portfolio-details-carousel">
					@if(!$product->image2 && !$product->image3)
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					@elseif(!$product->image3)
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					<img src="{{asset('images/product/'.$product->image2)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					@else
					<img src="{{asset('images/product/'.$product->image1)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					<img src="{{asset('images/product/'.$product->image2)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					<img src="{{asset('images/product/'.$product->image3)}}" class="img-fluid" alt="" style="height: 400px; border-radius: 20px">
					@endif
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<label><strong>Form Pesanan</strong></label>
			<table class="table" style="border-top: ; font-family: Calibri">
				<tr>
					<td>Nama Produk</td>
					<td>:</td>
					<td>{{$product->name}}</td>
				</tr>
				<tr>
					<td>Harga Produk</td>
					<td>:</td>
					<td>@currency($product->harga)</td>
				</tr>
				<tr>
					<td>Stok Produk</td>
					<td>:</td>
					<td>{{$product->stock}}</td>
				</tr>
				<tr>
					<td>Isi</td>
					<td>:</td>
					<td>{{$product->qty}}</td>
				</tr>
				<tr>
					<td>Berat Produk</td>
					<td>:</td>
					<td>{{number_format($product->berat_produk)}} gram</td>
				</tr>
				
				<tr>
					<td>Jumlah Pesanan</td>
					<td>:</td>
					<td>
						<form method="POST" action="/product/{{$product->id}}/checkout">
							@csrf
							<input type="text" class="form-control" id="jumlah_pesanan" value="{{old('jumlah_pesanan','')}}" name="jumlah_pesanan" required="">
							@error('jumlah_pesanan')
							<div class="alert alert-danger">{{ $message }}</div>
							@enderror

							<button type="submit" class="btn btn-dark btn-sm mt-3" style="font-family: calibri"><i class="fa fa-shopping-cart"></i> Masukkan Keranjang</button>
						</form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

@endsection