@extends('main.master2')

@section('content')
<!-- ======= Breadcrumbs Section ======= -->
<section class="breadcrumbs">
	<div class="container">

		<div class="d-flex justify-content-between align-items-center">
			<h2 style="font-weight: bold; text-shadow: 1px 1px 0.3px #000000; color: #bc8d42">Check Out Page</h2>
			<ol>
				<li><a href="/">Home</a></li>
				<li><a href="/our-products">Our Products</a></li>
				<li style="color: #bc8d42">Check Out</li>
			</ol>
		</div>
	</div>
</section><!-- End Breadcrumbs Section -->
<div class="container">
	<div class="card mt-4">
		<div class="card-header" style="background-color: #ff000017">
			<h3 style="font-family: calibri; color: #bc8d42"><i class="fa fa-shopping-cart"></i> Check Out Pesanan</h3>
			
		</div>
		<div class="card-body">
			<a href="raaina-shop" class="btn btn-outline-dark btn-sm mb-2"><i class="fa fa-shopping-cart"></i> Lanjut Belanja</a>
			<table class="table table-striped table-bordered" style="font-family: calibri">
				<thead>
					<tr>
						<th scope="col">No</th>
						<th scope="col">Nama Produk</th>
						<th scope="col">Harga</th>
						<th scope="col">Jumlah</th>
						<th scope="col">Total Harga</th>
						<th scope="col" width="20px">Action</th>
					</tr>
				</thead>
				<tbody>
					<td colspan="6" align="center">KERANJANG ANDA MASIH KOSONG</td>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection