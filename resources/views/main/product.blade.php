@extends('main.master2')

@section('content')
<main id="main">

  <!-- ======= Breadcrumbs Section ======= -->
 <!--  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2 style="font-weight: bold; text-shadow: 1px 1px 0.3px #000000; color: #bc8d42">Our Products</h2>
        <ol>
          <li><a href="/">Home</a></li>
          <li style="color: #bc8d42">Our Products</li>
        </ol>
      </div>

    </div>
  </section> --><!-- End Breadcrumbs Section -->

  <section class="jumbotron" style="background-image: url('{{asset('images/header/1606725888.jpg')}}'); background-size: cover; height: 100%; background-color: black">
    <div class="jumbotron" style="background-color: transparent;">
      <h1 class="display-4">Our Products</h1>
      <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
      <hr class="my-4">
      <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
      <a class="btn btn-outline-dark" href="#product" role="button">Learn more</a>
    </div>
  </section>

  <section id="product" class="portfolio section-bg" style="background: white">
    <div class="container" data-aos="fade-up" data-aos-delay="100">

      <style>
        .portfolio #portfolio-flters li{
          box-shadow: none;
        }
      </style>
      <div class="row">
        <div class="col-lg-12">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">Semua Produk</li>
            @foreach($category as $category2)
            <li data-filter=".filter-{{$category2->id}}">{{$category2->name}}</li>
            @endforeach
          </ul>
        </div>
      </div>

      <div class="row portfolio-container" style="text-align: -webkit-center">
        @foreach($product->take(20) as $product2)
        <div class="col-lg-3 col-md-6 portfolio-item filter-{{$product2->categories_id}}">
          <div class="card" style="width: 17rem; border-radius: 20px; border-width: 1px; border-color: #bc8d42">
            <img src="{{asset('images/product/'.$product2->image1)}}" class="card-img-top" alt="..." height="250px">
            <div class="card-body" style="text-align: left;">
              <a href="" class="card-text btn btn-sm mb-2" style="font-weight: normal; font-family: Calibri; border-color: #bc8d42;">{{ $product2->categories->name}}</a>
              <h5 class="card-title" style="font-weight: bold; color: #bc8d42; font-family: Comic Sans MS">{{$product2->name}}</h5>
              <p class="card-text" style="font-weight: normal; font-family: Calibri;">@currency($product2->harga)</p>
              <div class="row">
                @if($product2->stock == 'In Stock')
                <a href="product/{{$product2->id}}/checkout" class="btn btn-dark btn-sm ml-3" style="background-color: pink; color: black "><i class="fas fa-shopping-cart"></i> Pesan</i></a>
                @else
                <a href="#" class="btn btn-danger btn-sm ml-3"><i class="fas fa-ban"></i> Stok Kosong</i></a>
                @endif
                <a href="/product-detail/{{$product2->id}}" class="btn btn-link btn-sm ml-5" style="color: black"><i class="fas fa-eye"></i> Detail</i></a>
              </div>

              <li class="dropdown-divider" style="border-color: #000"></li>
              <a href="#" class="col btn btn-outline-warning btn-sm mb-1" style="border-radius: 20px"><i class="fas fa-cart-plus"> </i> Shopee</i></a>
              <a href="#" class="col btn btn-outline-success btn-sm mb-1" style="border-radius: 20px"><i class="fas fa-cart-plus"> </i> Tokopedia</i></a>
              <a href="#" class="col btn btn-outline-danger btn-sm" style="border-radius: 20px"><i class="fas fa-cart-plus"> </i> Bukalapak</i></a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

    </div>
  </section><!-- End Our Portfolio Section -->


</main><!-- End #main -->
@endsection