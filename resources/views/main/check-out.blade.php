@extends('main.master2')

@section('content')
<!-- ======= Breadcrumbs Section ======= -->
<section class="breadcrumbs">
	<div class="container">

		<div class="d-flex justify-content-between align-items-center">
			<h2 style="font-weight: bold; text-shadow: 1px 1px 0.3px #000000; color: #bc8d42">Check Out Page</h2>
			<ol>
				<li><a href="/">Home</a></li>
				<li><a href="/our-products">Our Products</a></li>
				<li style="color: #bc8d42">Check Out</li>
			</ol>
		</div>
	</div>
</section><!-- End Breadcrumbs Section -->
<div class="container">
	<div class="card mt-4">
		<div class="card-header" style="background-color: #ff000017">
			<h3 style="font-family: calibri; color: #bc8d42"><i class="fa fa-shopping-cart"></i> Check Out Pesanan</h3>
			@if(!empty($order))
			<p style="font-size: 12px">Order Date : {{now()->isoFormat('D MMMM Y')}}</p>
		</div>
		<div class="card-body">
			<a href="raaina-shop" class="btn btn-outline-dark btn-sm mb-2"><i class="fa fa-shopping-cart"></i> Lanjut Belanja</a>
			<table class="table table-striped table-bordered" style="font-family: calibri">
				<thead>
					<tr>
						<th scope="col">No</th>
						<th scope="col">Nama Produk</th>
						<th scope="col">Harga</th>
						<th scope="col">Jumlah</th>
						<th scope="col">Total Harga</th>
						<th scope="col" width="20px">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; ?>
					@foreach($order_detail as $order_detail2)
					<tr>
						<th scope="row">{{$no++}}</th>
						<td>{{$order_detail2->product->name}}</td>
						<td>@currency($order_detail2->product->harga)</td>
						<td>{{$order_detail2->jumlah_pesanan}}</td>
						<td>@currency($order_detail2->total_harga)</td>
						<td align="center">
							<form action="/check-out/{{$order_detail2->id}}" method="post" enctype="multipart-data">
								@csrf
								@method('delete')
								<button type="submit" class="btn btn-outline-dark btn-sm"><i class="fa fa-trash"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
					@if(!$order->total_harga)
					<td colspan="6" align="center">KERANJANG ANDA MASIH KOSONG</td>
					@else
					<tr>
						<td colspan="4" align="right"><strong>Total Harga</strong></td>
						<td colspan="2">@currency($order->total_harga)</td>
					</tr>
					<tr>
						<form role="form" action="/confirm-check-out" method="POST" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<td colspan="6">
								<div class="card-body">
									<label class="mb-4" style="text-decoration: underline; font-weight: bold; font-size: 20px">Silahkan Lengkapi Form dibawah ini :)</label>

									<div class="form-group">
										<label for="nama_penerima">Nama Penerima</label>
										<input type="text" class="form-control" id="nama_penerima" value="{{old('nama_penerima',Auth::user()->profiles->nama_lengkap)}}" name="nama_penerima" >
										@error('nama_penerima')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>

									<div class="form-group">
										<label for="no_hp">No Handphone</label>
										<input type="text" class="form-control" id="no_hp" value="{{old('no_hp',Auth::user()->profiles->no_hp)}}" name="no_hp" >
										@error('no_hp')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>

									<!-- <div class="form-group">
										<label for="provinsi">Provinsi</label>
										<input type="text" class="form-control" id="provinsi" value="{{old('provinsi',$order_detail2->provinsi)}}" name="provinsi" >
										@error('provinsi')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div> -->


									<div class="form-group">
										<label for="provinsi">Provinsi</label>
										<select class="custom-select provinsi" id="provinsi" name="provinsi">
											<option value="0" disabled="true" selected="true">-- Pilih Provinsi --</option>
											@foreach($province as $provinsi2)
											<option id="provinsi" name="provinsi" value="{{ $provinsi2->id }}">{{ $provinsi2->province }}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<label for="kotaname">Kota</label>
										<select class="custom-select kotaname" id="kotaname" name="kotaname">
											<option value="0" disabled="true" selected="true">Kota</option>
										</select>
									</div>

									

									<div class="form-group">
										<label for="kecamatan">Kecamatan</label>
										<input type="text" class="form-control" id="kecamatan" value="{{old('kecamatan',$order_detail2->kecamatan)}}" name="kecamatan" >
										@error('kecamatan')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>

									<div class="form-group">
										<label for="kode_pos">Kode POS</label>
										<input type="number" class="form-control" id="kode_pos" value="{{old('kode_pos',$order_detail2->kode_pos)}}" name="kode_pos" >
										@error('kode_pos')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>

									<div class="form-group">
										<label for="alamat">Alamat Lengkap Penerima</label>
										<textarea type="text" class="form-control" id="alamat" value="{{old('alamat',Auth::user()->profiles->alamat_pengiriman)}}" name="alamat" style="height: 100px">{{Auth::user()->profiles->alamat_pengiriman}}</textarea>
										@error('alamat')
										<div class="alert alert-danger">{{ $message }}</div>
										@enderror
									</div>

								</div>
							</td>

							<tr>
								<td colspan="6" align="right">
									<button type="submit" class="btn btn-outline-dark" style="background-color: pink"><i class="fa fa-shopping-bag"></i> Check-Out</button>
								</td>
							</tr>
						</form>
						@endif
					</tr>
				</tbody>
			</table>
			@endif
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){

		$(document).on('change','.provinsi', function(){
			// console.log("berubah");

			var province_id=$(this).val();
			// console.log(province_id);
			var div=$(this).parent();

			var op=" ";

			$.ajax({
				type:'get',
				url:'{!!URL::to('findKotaName')!!}',
				data:{'id':province_id},
				success:function(data){
					console.log('success');
					console.log(data);

					op+='<option value="0" selected disabled>pilih kota</option>';
					for (var i = 0; i < data.length; i++) {
						op+='<option value="'+data[i].id+'">'+data[i].city_name+'</option>';
					}

					div.find('#kotaname').html(" ");
					div.find('#kotaname').append(op);
				},
				error:function(){

				}
			});
		});
	});
</script>


@endsection