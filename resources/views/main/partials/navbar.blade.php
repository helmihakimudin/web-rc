  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">contact@example.com</a>
        <i class="icofont-phone"></i> <a>+1 5589 55488 55</a>
      </div>
      <div class="social-links float-right">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <!-- <a href="#" class="skype"><i class="icofont-skype"></i></a>
          <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> -->
        </div>
      </div>
    </section>

    <!-- ======= Header ======= -->
    <header id="header">
      <div class="container">

        <div class="logo float-left">
          <div class="row">
            <a href="/"><img src="{{asset('/images/logo-raaina.png')}}" alt="" class="img-fluid mt-1 mb-1 mr-2" style="height: 50px; width: 50px;"></a>
            <h1 style="text-shadow: 2px 2px 3px #ccc;" class="text-light"><a href="/"><span>Raaina</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->

          </div>
        </div>

        <nav class="nav-menu float-right d-none d-lg-block">
          <ul>
            <li class="active"><a href="#header">Home</a></li>
            <li><a href="#about">About Us</a></li>
            <li><a href="#ingredients">Ingredients Knowledge</a></li>
            <li><a href="#product">Our Products</a></li>
            <li><a href="#team">Team</a></li>
         <!--  <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li> -->
          <li><a href="#contact">Contact Us</a></li>
          @guest
          <li class="active"><a href="{{ route('login') }}" style="color: #ff469d">Login</a></li>
          @if (Route::has('register'))
          <li class="active"><a href="{{ route('register') }}" style="color: #ff469d">Register</a></li>
          @endif
          @else

          @if (!Auth::user()->profile)
          <li class="drop-down"><a href="#">{{ Auth::user()->name }}</a>
            @else
            <li class="drop-down"><a href="#">{{ Auth::user()->profile->nama }}</a>
              <li class="drop-down"><a href="#" class="instagram"><i class="icofont-instagram"></i></a></li>
              @endif
              <ul>
                <li><a href="/admin">Administrator</a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a></li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </ul>
            </li>
            <?php   
            $pesanan_utama = \App\Models\Order::where('user_id', Auth::user()->id)->where('status',0)->first();
            if (!empty($pesanan_utama)) {
             $notif = \App\Models\Detail::where('order_id', $pesanan_utama->id)->count();
           }

           ?>
           <li>@if(!empty($order))
            <a href="/check-out" class="cart">
              @else
              <a href="/keranjang-kosong" class="cart">
                @endif
                <i class="fa fa-shopping-cart" style="font-size: 20px"></i>
                @if(!empty($notif))
                <span class="badge badge-danger" style="font-size: 14px; font-family: calibri; width: 20px; border-radius: 20px">{{$notif}}</span></a></li>
                @else
                <span class="badge badge-danger" style="font-size: 14px; font-family: calibri; width: 20px; border-radius: 20px">0</span></a></li>
                @endif
                @endguest
              </ul>
            </nav><!-- .nav-menu -->

          </div>
  </header><!-- End Header -->