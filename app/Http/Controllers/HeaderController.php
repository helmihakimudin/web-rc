<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Header;

class HeaderController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

    	public function edit($id)
    {
    	$header = Header::find($id);

    	return view('admin.header.edit', compact('header'));
    }

        public function index()
    {
    	$header = Header::all();
    	return view('admin.header.index', compact('header'));
    }

        public function update(Request $request, $id)
    {
    	$header = Header::find($id);

    	if ($request->hasFile('images')) {
            $file = $request->file('images');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/header/', $filename);
            $header->images = $filename;
            # code...
        }

        $header->save();
    	return redirect('/admin/header')->with('success', 'Image Header Berhasil di Update!');
    }
}
