<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\Header;
use App\Models\Ingredient;
use App\Models\Category;
use App\Models\Order;
use App\Models\Detail;
use App\Models\City;
use App\Models\Province;
use Auth;
use Alert;
use Kavist\RajaOngkir\Facades\RajaOngkir;

class MainController extends Controller
{
	public function index()
	{
		$header = Header::all();
		$category = Category::all();
		$ingredient = Ingredient::orderBy('name')->get();
		$product = Product::all();
		return view('main.index', compact('header', 'ingredient', 'product', 'category'));
	}

	public function product()
	{
		$header = Header::all();
		$category = Category::all();
		$ingredient = Ingredient::orderBy('name')->get();
		$product = Product::all();
		return view('main.product', compact('header', 'ingredient', 'product', 'category'));
	}

	public function productDetail($id)
	{
		$product = Product::find($id);

		return view('main.detail-product', compact('product'));
	}

	public function productBeli($id)
	{
		$product = Product::find($id);

		return view('main.beli-product', compact('product'));
	}

	public function pesan(Request $request, $id)
	{
		$product = Product::where('id', $id)->first();

		//cek validasi jika pesanan sudah ada, tidak buat baru
		$cek_pesanan = Order::where('user_id', Auth::id())->where('status',0)->first();
		//simpan order
		if (empty($cek_pesanan)) {
			$order = new Order;
			$order->user_id = Auth::id();
			$order->status = 0;
			$order->total_harga = 0;
			
			$order->save();
		}

		// Deklarasi order_id dari database
		$pesanan_baru = Order::where('user_id', Auth::id())->where('status',0)->first();


		// cek oesanan detail
		$cek_detail = Detail::where('product_id', $product->id)->where('order_id', $pesanan_baru->id)->first();

		if (empty($cek_detail)) 
		{
			//simpan detail
			$detail = new Detail;
			$detail->jumlah_pesanan = $request->jumlah_pesanan;
			$detail->total_harga = $product->harga*$request->jumlah_pesanan;
			$detail->product_id = $product->id;
			$detail->order_id = $pesanan_baru->id;
			$detail->save();
		}else{
			$detail = Detail::where('product_id', $product->id)->where('order_id', $pesanan_baru->id)->first();
			$detail->jumlah_pesanan = $detail->jumlah_pesanan+$request->jumlah_pesanan;

			//harga sekarang
			$harga_detail_baru = $product->harga*$request->jumlah_pesanan;
			$detail->total_harga = $detail->total_harga+$harga_detail_baru;
			$detail->update();
		}
		
		$order = Order::where('user_id', Auth::id())->where('status',0)->first();
		$order->total_harga = $order->total_harga+$product->harga*$request->jumlah_pesanan;
		$order->update();

		Alert::success('Pesanan Sukses Masuk Keranjang', 'Success');
		return redirect('/check-out');
	}

	//
	
	//

	public function checkout()
	{
		$province = Province::all();
		$city = City::all();
		
		$order = Order::where('user_id', Auth::id())->where('status',0)->first();
		if (!empty($order)) {
			$order_detail = Detail::where('order_id', $order->id)->get();
		}
		

		return view('main.check-out', compact('order','order_detail', 'province', 'city'));
	}

	public function findKotaName(Request $request){
		$data = City::select('city_name', 'id')->where('province_id',$request->id)->get();
		return response()->json($data);
	}

	public function checkoutDelete($id)
	{
		$order_detail = Detail::where('id',$id)->first();

		$order = Order::where('id', $order_detail->order_id)->first();
		$order->total_harga = $order->total_harga-$order_detail->total_harga;
		$order->update();

		$order_detail->delete();

		Alert::error('Pesanan Sukses Dihapus', 'Success');
		return redirect('/check-out');

	}

	public function checkoutConfirm()
	{
		// $request->validate([
		// 	'nama_penerima'=>'required',
		// 	'no_hp'=>'required',
		// 	'provinsi'=>'required',
		// 	'kota'=>'required',
		// 	'kecamatan'=>'required',
		// 	'kode_pos'=>'required',
		// 	'alamat_penerima'=>'required'

		// ]);
	
		$order = Order::where('user_id', Auth::id())->where('status',0)->first();

		$order->status = 1;
		$order->update();

		Alert::success('Pesanan Sukses di Check-Out', 'Success');
		return redirect('/check-order');
	}

	public function checkoutConfirmPut(Request $request)
	{
		$request->validate([
			'nama_penerima'=>'required',
			'no_hp'=>'required',
			'provinsi'=>'required',
			'kota'=>'required',
			'kecamatan'=>'required',
			'kode_pos'=>'required',
			'alamat'=>'required'

		]);
	
		$order = Order::where('user_id', Auth::id())->where('status',0)->first();

		$order->nama_penerima = $request->nama_penerima;
		$order->no_hp = $request->no_hp;
		$order->provinsi = $request->provinsi;
		$order->kota = $request->kota;
		$order->kecamatan = $request->kecamatan;
		$order->kode_pos = $request->kode_pos;
		$order->alamat = $request->alamat;
		$order->status = 1;
		$order->save();

		Alert::success('Pesanan Sukses di Check-Out', 'Success');
		return redirect('/raaina-shop');
	}

	public function checkOrder($id)
	{
		$order_check = find($id);

		return view('main.order-check', compact('order_check'));
	}

	public function getProvince()
	{

		$provinces = Province::pluck('province', 'id');
		return view('check-out', compact('provinces'));
	}

	public function getCities($id)
	{
		$city = City::where('province_id', $id)->pluck('city_name', 'id');
		return json_encode($city);
	}

	public function check_ongkir(Request $request)
	{
		$cost = RajaOngkir::ongkosKirim([
            'origin'        => 22, // ID kota/kabupaten asal
            'destination'   => $request->kota, // ID kota/kabupaten tujuan
            'weight'        => 1000, // berat barang dalam gram
            'courier'       => 'jne' // kode kurir pengiriman: ['jne', 'tiki', 'pos'] untuk starter
        ])->get();


		
	}

	
}
