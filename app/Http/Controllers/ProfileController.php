<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Profile;
use Auth;

class ProfileController extends Controller
{
	public function create()
	{

		return view('admin.profile.create');
	}

	public function store(Request $request)
	{
		$request->validate([
			'nama_lengkap'=>'required',
			'tanggal_lahir'=>'required',
			'no_hp'=>'required',
			'alamat_pengiriman'=>'required'

		]);
		$profile = new Profile;

		$profile->nama_lengkap = $request->nama_lengkap;
		$profile->tanggal_lahir = $request->tanggal_lahir;
		$profile->no_hp = $request->no_hp;
		$profile->alamat_pengiriman = $request->alamat_pengiriman;
		$profile->user_id = Auth::id();
		if ($request->hasFile('avatar')) {
			$file = $request->file('avatar');
			$extension = $file->getClientOriginalExtension();
			$filename = time() . '.' . $extension;
			$file->move('images/profile/', $filename);
			$profile->avatar = $filename;
            # code...
		}

		$profile->save();
		return redirect('/admin');
	}

	public function edit($id)
	{
		$profile = Profile::find($id);

		return view('admin.profile.edit', compact('profile'));
	}

	public function update(Request $request, $id)
	{
		$request->validate([
			'nama_lengkap'=>'required',
			'tanggal_lahir'=>'required',
			'no_hp'=>'required',
			'alamat_pengiriman'=>'required'

		]);
		$profile = Profile::find($id);

		$profile->nama_lengkap = $request->nama_lengkap;
		$profile->tanggal_lahir = $request->tanggal_lahir;
		$profile->no_hp = $request->no_hp;
		$profile->alamat_pengiriman = $request->alamat_pengiriman;
		$profile->user_id = Auth::id();
		if ($request->hasFile('avatar')) {
			$file = $request->file('avatar');
			$extension = $file->getClientOriginalExtension();
			$filename = time() . '.' . $extension;
			$file->move('images/profile/', $filename);
			$profile->avatar = $filename;
            # code...
		}

		$profile->save();
		return redirect('/admin');
	}

}
