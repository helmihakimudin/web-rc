<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class getApiController extends Controller
{
	public function cost()
	{
		$response = Http::withHeaders([
			'key'=>'8253dfef51b788500ac968074ddda774'
		])->get('https://api.rajaongkir.com/starter/province');
		return $response->json();
	}

	public function province()
	{

		$origin = 501;
		$destination = 114;
		$weight = 1700;
		$courier = 'jne';

		$response = Http::asForm()->withHeaders([
			'key'=>'8253dfef51b788500ac968074ddda774'
		])->post('https://api.rajaongkir.com/starter/cost', [
			'origin' => $origin,
			'destination' => $destination,
			'weight' => $weight,
			'courier' => $courier
		]);

		return $response->json();
	}

	public function getCourier(Request $request)
	{
		$this->validate($request, [
			'destination' => 'required',
			'weight' => 'required|integer'
		]);

    //MENGIRIM PERMINTAAN KE API RUANGAPI UNTUK MENGAMBIL DATA ONGKOS KIRIM
    //BACA DOKUMENTASI UNTUK PENJELASAN LEBIH LANJUT
		$url = 'https://ruangapi.com/api/v1/shipping';
		$client = new Client();
		$response = $client->request('POST', $url, [
			'headers' => [
				'Authorization' => '8253dfef51b788500ac968074ddda774'
			],
			'form_params' => [
            'origin' => 22, //ASAL PENGIRIMAN, 22 = BANDUNG
            'destination' => $request->destination,
            'weight' => $request->weight,
            'courier' => 'jne,jnt' //MASUKKAN KEY KURIR LAINNYA JIKA INGIN MENDAPATKAN DATA ONGKIR DARI KURIR YANG LAIN
        ]
    ]);

		$body = json_decode($response->getBody(), true);
		return $body;
	}

}
