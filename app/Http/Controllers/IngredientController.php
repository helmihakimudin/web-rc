<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Ingredient;

class IngredientController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function create()
	{
		return view('admin.ingredient.create');
	}

	public function store(Request $request)
	{
		$request->validate([
			'name'=>'required',
			'description'=>'required'

		]);
		$ingredient = new Ingredient;

		$ingredient->name = $request->name;
		$ingredient->description = $request->description;

		$ingredient->save();

		return redirect('/admin/ingredient')->with('success', 'Data Ingredients Berhasil dibuat!');
	}

	public function index()
	{
		$ingredient = DB::table('ingredients')->orderBy('name')->paginate(10);

		return view('admin.ingredient.index', compact('ingredient'));
	}

	public function edit($id)
	{
		$ingredient = Ingredient::find($id);

		return view('admin.ingredient.edit', compact('ingredient'));
	}

	public function update(Request $request, $id)
	{
		$request->validate([
			'name'=>'required',
			'description'=>'required'

		]);
		$ingredient = Ingredient::find($id);

		$ingredient->name = $request->name;
		$ingredient->description = $request->description;

		$ingredient->save();

		return redirect('/admin/ingredient')->with('success', 'Data Ingredients Berhasil di Update!');
	}

	public function destroy($id)
	{
		$ingredient = DB::table('ingredients')->where('id', $id)->delete();

		return redirect('/admin/ingredient');
	}


	public function show($id)
	{
		$ingredient = Ingredient::find($id);

		return view('admin.ingredient.show', compact('ingredient'));
	}
}
