<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Category;

class CategoriesController extends Controller
{
	public function create()
	{
		return view('admin.category.create');
	}   

	public function store(Request $request)
	{
		$request->validate([
			'name'=>'required'

		]);
		$category = new Category;

		$category->name = $request->name;

		$category->save();

		return redirect('/admin/category');
	}

	public function index()
	{
		$category = DB::table('categories')->orderBy('name')->paginate(10);

		return view('admin.category.index', compact('category'));
	}

	public function edit($id)
	{
		$category = Category::find($id);

		return view('admin.category.edit', compact('category'));
	}

	public function update(Request $request, $id)
	{
		$request->validate([
			'name'=>'required'

		]);
		$category = Category::find($id);

		$category->name = $request->name;

		$category->save();

		return redirect('/admin/category');
	}

	public function destroy($id)
	{
		$category = DB::table('categories')->where('id', $id)->delete();

		return redirect('admin/category')->with('success', 'Berhasil Delete');
	}

}
