<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\Header;
use App\Models\Ingredient;
use App\Models\Category;
use App\Models\Order;
use App\Models\Detail;
use App\Models\City;
use App\Models\Province;
use Auth;
use Alert;
use Kavist\RajaOngkir\Facades\RajaOngkir;


class DataOrderController extends Controller
{
	public function index()
	{
		$order = Order::where('status', 1)->orderBy('id')->paginate(20);
		$detail = Detail::paginate(20);

		return view('admin.data-order.index', compact('order','detail'));

	}

	public function edit($id)
	{
		$status = Order::find($id);

		return view('admin.data-order.edit', compact('status'));
	}

	public function update(Request $request, $id)
	{
		$status = Order::find($id);

		$status->order_status = $request->order_status;

		$status->save();

		return redirect('admin.data-order');
	}

	public function show($id)
	{
		$order = Order::find($id);

		$detail_show = Detail::where('order_id', $order->id)->get();


		return view('admin.data-order.detail', compact('detail_show', 'order'));

	}

	public function orders()
	{
		$order = Order::orderBy('id')->where('user_id', Auth::id())->where('status', 1)->where('order_status', '!=' , 'Transaksi Selesai')->paginate(20);
		$detail = Detail::paginate(20);

		return view('admin.data-order-buyer.index', compact('order','detail'));
	}

	public function history()
	{
		$order = Order::orderBy('id')->where('user_id', Auth::id())->paginate(20);
		$detail = Detail::paginate(20);
		$history = DB::table('orders')->where('order_status', 'Transaksi Selesai')->get();

		return view('admin.data-order-buyer.history', compact('order','detail', 'history'));
	}

	public function buktiPembayaran($id)
	{
		$bukti = Order::find($id);

		return view('admin.data-order-buyer.bukti', compact('bukti'));
	}

	public function upload(Request $request, $id)
	{
		$bukti = Order::find($id);
		
		if ($request->hasFile('bukti_pembayaran')) {
            $file = $request->file('bukti_pembayaran');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/bukti/', $filename);
            $bukti->bukti_pembayaran = $filename;
            # code...
        }

        $bukti->save();

        return redirect('/admin/orders');
	}
}
