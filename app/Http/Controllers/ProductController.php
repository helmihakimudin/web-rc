<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Product;
use App\Models\Category;
use App\Models\Ingredient;
use Auth;

class ProductController extends Controller
{
    public function create()
    {
        $category = Category::all();
        $ingredient = Ingredient::all();

    	return view('admin.product.create', compact('category','ingredient'));
    }

    public function store(Request $request)
    {
        $request->validate([
                'name'=>'required|unique:products',
                'categories_id'=>'required',
                'ingredient'=>'required',
                'qty'=>'required',
                'berat_produk'=>'required',
                'harga'=>'required'

            ]);

    	$product = new Product;

    	$product->name = $request->name;
        $product->categories_id = $request->categories_id;
        $product->qty = $request->qty;
        $product->dimensi = $request->dimensi;
        $product->berat_produk = $request->berat_produk;
        $product->manfaat = $request->manfaat;
        $product->cara_penggunaan = $request->cara_penggunaan;
        $product->harga = $request->harga;
        $product->stock = $request->stock;

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image1 = $filename;
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image2 = $filename;
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image3 = $filename;
        }

        $product->save();
        $product->ingredients()->attach($request->ingredient);

        return redirect('/admin/product')->with('success', 'Product Berhasil di Tambah!');
    }

    public function index(){
        
        $product = Product::orderBy('name')->paginate(10);
        return view('admin.product.index', compact('product'));
    }

    public function edit($id){
        
        $category = Category::all();
        $ingredient = Ingredient::all();
        $product = Product::find($id);

        return view('admin.product.edit', compact('product','category', 'ingredient'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
                'name'=>'required',
                'categories_id'=>'required',
                'ingredient'=>'required',
                'qty'=>'required',
                'berat_produk'=>'required',
                'harga'=>'required'

            ]);

        $product = Product::find($id);

        $product->name = $request->name;
        $product->categories_id = $request->categories_id;
        $product->qty = $request->qty;
        $product->dimensi = $request->dimensi;
        $product->berat_produk = $request->berat_produk;
        $product->manfaat = $request->manfaat;
        $product->cara_penggunaan = $request->cara_penggunaan;
        $product->harga = $request->harga;
        $product->stock = $request->stock;

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image1 = $filename;
        }

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image2 = $filename;
        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images/product/', $filename);
            $product->image3 = $filename;
        }

        $product->save();
        $product->ingredients()->sync($request->ingredient);

        return redirect('/admin/product')->with('success', 'Product Berhasil di Tambah!');
    }

    public function destroy($id)
    {
        $product = DB::table('products')->where('id', $id)->delete();

        return redirect('/admin/product');
    }
}
